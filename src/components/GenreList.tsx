import React from "react";
import useGenre, { Genre } from "../hooks/useGenre";
import useData from "../hooks/useData";
import { HStack, List, ListItem, Image, Text, Spinner, Button } from "@chakra-ui/react";
import getCropImageUrl from "../services/image-url";


interface Props {
  onSelectGenre: (genre: Genre) => void;
  selectedGenre: Genre | null;
}
const GenreList = ({onSelectGenre, selectedGenre} : Props) => {
  const { data, isLoading } = useGenre();
  if (isLoading) return <Spinner></Spinner>;
  return (
    <List>
      {data.map((genre) => (
        <ListItem key={genre.id} paddingY={"5px"}>
          <HStack>
            <Image
              boxSize={"32px"}
              borderRadius={"8px"}
              src={getCropImageUrl(genre.image_background)}
            ></Image>
            <Button fontWeight={genre.id === selectedGenre?.id ? 'bold' : 'normal'} onClick={() => onSelectGenre(genre)} fontSize={'lg'} variant={"link"}>{genre.name}</Button>
          </HStack>
        </ListItem>
      ))}
    </List>
  );
};

export default GenreList;
